#include "mainwindow.h"
#include "modelo.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Modelo m;
    MainWindow w(&m);
    w.show();

    return a.exec();
}
