#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(Modelo *m, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    modelo = m;
    ui->setupUi(this);
    m_label = findChild<QLabel*>("label");
    m_label->setAlignment(Qt::AlignCenter);

    modeloToView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonCerrar_clicked()
{
    QApplication::quit();
}

void MainWindow::on_pushButtonSumar_clicked()
{
    modelo->incrementar();
    modeloToView();
}

void MainWindow::on_pushButtonRestar_clicked()
{
    modelo->decrementar();
    modeloToView();
}

void MainWindow::modeloToView(void)
{
    std::cout << modelo->getValor() << std::endl;

    QString s;
    s.setNum(modelo->getValor());
    s.prepend("000");
    s = s.right(3);

    m_label->setText(s);
}
