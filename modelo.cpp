#include "modelo.h"

Modelo::Modelo()
{
    valor = 0;
}

void Modelo::incrementar()
{
    if(valor < 100)
    {
        valor++;
    }
}
void Modelo::decrementar()
{
    if(valor > 0)
    {
        valor--;
    }
}

int Modelo::getValor()
{
    return valor;
}
