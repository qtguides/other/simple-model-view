#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "modelo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Modelo *m,QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonCerrar_clicked();

    void on_pushButtonSumar_clicked();

    void on_pushButtonRestar_clicked();

private:
    Ui::MainWindow *ui;
    QLabel *m_label;
    Modelo *modelo;

    void modeloToView(void);
};

#endif // MAINWINDOW_H
