#ifndef MODELO_H
#define MODELO_H


class Modelo
{
private:
    int valor;

public:
    Modelo();
    void incrementar();
    void decrementar();
    int getValor();
};

#endif // MODELO_H
